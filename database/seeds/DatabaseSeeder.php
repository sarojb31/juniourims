<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {


        \App\Role::insert([
            'name' => 'Admin'
        ]);

        \App\Role::insert([
            'name' => 'Staff'
        ]);
        \App\Role::insert([
            'name' => 'Teacher'
        ]);
        \App\Role::insert([
            'name' => 'Student'
        ]);

        \App\User::insert([
            'name' => 'IMS Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password'),
            'phone' => '9803961735',
            'address' => 'Anamanagar, Kathamandu',
            'role_id' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \App\SiteSetting::insert([
            'company_name' => 'Green Computing Nepal'
        ]);

        \App\Shift::insert([
            ['shift_available'=>'Morning'],
            ['shift_available'=>'Day'],
            ['shift_available'=>'Evening'],
            ['shift_available'=>'Night'],
        ]);

         \App\Section::insert([
            ['section_name'=>'A'],
            ['section_name'=>'B'],

        ]);
    }
}
