<?php

namespace App\Http\Controllers;
use App\Section;

use Illuminate\Http\Request;

class SectionController extends Controller
{
    // Add Course
    public function addSection(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $section = new Section;
            $section->section_name = $data['name'];
            

            $section->save();
            return redirect()->route('viewSections')->with('flash_message', 'New Section Has Been Added');

        }
        return view ('admin.section.add');
    }

     // View Courses
    public function viewSections(){
        $sections = Section::latest()->get();
        return view ('admin.section.view', compact('sections'));
    }

     // Edit Course
		public function editSection(Request $request, $id){
        $section = Section::findOrFail($id);
        if($request->isMethod('post')){
            $data = $request->all();
            $section->section_name = $data['name'];
           

           
            $section->save();

            
            

            return redirect()->route('viewSections')->with('flash_message', 'Section Has Been Updated');

        }

        return view ('admin.section.edit', compact('section'));
    }

     // Delete Section
     public function deleteSection($id){
        $section = Section::findOrFail($id);
        $section->delete();

        $image_path = 'public/uploads/section/';

        if(!empty($section->image)){
            if(file_exists($image_path.$section->image)){
                unlink($image_path.$section->image);
            }
        }

        return redirect()->route('viewSections')->with('flash_message', 'Section Has Been Deleted');
    }

}
