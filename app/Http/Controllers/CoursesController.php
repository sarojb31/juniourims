<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class CoursesController extends Controller
{
    // Add Course
    public function addCourse(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $course = new Course;
            $course->name = $data['name'];
            $course->code = $course->getLatestCourseNumber();
            $course->slug = str_slug($data['name']);
            $course->fees = $data['fees'];
            $course->duration = $data['duration'];

            if(empty($data['description'])){
                $course->description = "";
            } else {
                $course->description = $data['description'];
            }

            if (empty($data['status'])){
                $course->status = 0;
            } else {
                $course->status = 1;
            }


            $random = str_random(20);
            if($request->hasFile('image')){
                $image_tmp = Input::file('image');
                if($image_tmp->isValid()){
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = $random.'.'.$extension;
                    $image_path = 'public/uploads/course/'. $filename;
                    // Resize Image Code
                    Image::make($image_tmp)->save($image_path);
                    // Store image name in products table
                    $course->image = $filename;
                }
            }

            $course->save();
            return redirect()->route('viewCourses')->with('flash_message', 'New Course Has Been Added');

        }
        return view ('admin.course.add');
    }

    // View Courses
    public function viewCourses(){
        $courses = Course::latest()->get();
        return view ('admin.course.view', compact('courses'));
    }

    // Edit Course
    public function editCourse(Request $request, $id){
        $course = Course::findOrFail($id);
        if($request->isMethod('post')){
            $data = $request->all();
            $course->name = $data['name'];
            $course->code = $course->getLatestCourseNumber();
            $course->slug = str_slug($data['name']);
            $course->fees = $data['fees'];
            $course->duration = $data['duration'];

            if(empty($data['description'])){
                $course->description = "";
            } else {
                $course->description = $data['description'];
            }

            if (empty($data['status'])){
                $course->status = 0;
            } else {
                $course->status = 1;
            }


            $random = str_random(20);
            if($request->hasFile('image')){
                $image_tmp = Input::file('image');
                if($image_tmp->isValid()){
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = $random.'.'.$extension;
                    $image_path = 'public/uploads/course/'. $filename;
                    // Resize Image Code
                    Image::make($image_tmp)->save($image_path);
                    // Store image name in products table
                    $course->image = $filename;
                }
            }

            $course->save();

            $image_path = 'public/uploads/course/';
            if (file_exists($image_path.$data['current_image'])){
                if(!empty($data['image'])){
                    if (file_exists($image_path.$course->image)){
                        unlink($image_path.$data['current_image']);
                    }
                }
            }

            return redirect()->route('viewCourses')->with('flash_message', 'Course Has Been Updated');

        }
        return view ('admin.course.edit', compact('course'));
    }

    // Delete Course
    public function deleteCourse($id){
        $course = Course::findOrFail($id);
        $course->delete();

        $image_path = 'public/uploads/course/';

        if(!empty($course->image)){
            if(file_exists($image_path.$course->image)){
                unlink($image_path.$course->image);
            }
        }

        return redirect()->route('viewCourses')->with('flash_message', 'Course Has Been Deleted');
    }
}


