<?php


namespace App\Http\Controllers;

use App\Batch;
use App\Course;
use Illuminate\Http\Request;

class BatchController extends Controller
{
    //Add Batch
    public function addBatch(Request $request)
    {

        if ($request->isMethod('post')) {
            $data = $request->all();
            dd($data);
            $batch = new Batch;
            $batch->batch_name = $data['name'];
            $batch->year = $data['year'];
            $batch->month = $data['month'];
            $batch->save();

            return redirect()->route('viewBatches')->with('flash_message', 'New Batch Has Been Added');

        }

        $course = Course::latest()->get();
        return view('admin.batch.add',compact('course'));
    }

    //View Batches

    public function viewBatches()
    {
        $batch = Batch::latest()->get();
        return view('admin.batch.view', compact('batch'));
    }

    //Delete Batches

    public function deleteBatch($id)
    {

        $batch = Batch::findOrFail($id);
        $batch->delete();
        return redirect()->route('viewBatches')->with('flash_message', ' Batch Has Been Deleted');
    }

    //Edit Batch
    public function editBatch(Request $request, $id)
    {

        $batch = Batch::findOrFail($id);

        if ($request->isMethod('post')) {
            $data = $request->all();
            $batch->batch_name = $data['name'];
            $batch->year = $data['year'];
            $batch->month = $data['month'];
            $batch->save();

            return redirect()->route('viewBatches')->with('flash_message', 'Batch Has Been Updated');

        }

        return view('admin.batch.edit', compact('batch'));
    }

}
