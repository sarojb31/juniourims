<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Login
Route::match(['get', 'post'], '/ims-login', 'AdminLoginController@login')->name('adminlogin');


Route::group(['middleware' => ['adminlogin']], function () {
// Dashboard
    Route::get('/admin/dashboard', 'AdminController@dashboard')->name('admin.dashboard');

    // Profile
    Route::get('/admin/profile', 'AdminController@profile')->name('admin.profile');

    // Profile Update
    Route::post('/admin/profile/update/{id}', 'AdminController@updateProfile')->name('updateProfile');

    // Change Password
    Route::get('/admin/profile/change_password', 'AdminController@changePassword')->name('changePassword');

    // Checking User Password
    Route::post('/admin/profile/check-password', 'AdminController@chkUserPassword');

    // Update Password
    Route::post('/admin/profile/update_password/{id}', 'AdminController@updatePassword')->name('updatePassword');



    // User Management Routes
    Route::match(['get', 'post'],'/add/user', 'UsersController@addUser')->name('addUser');
    Route::get('/users/view_u/all', 'UsersController@viewAllUsers')->name('viewAllUsers');
    Route::match(['get', 'post'],'/edit/user/{id}', 'UsersController@editUser')->name('editUser');
    Route::get('trash-user/{id}', 'UsersController@trashUser')->name('trashUser');
    Route::get('/users/view_trashed', 'UsersController@viewTrashedUser')->name('viewTrashedUser');
    Route::get('/users/restore/{id}', 'UsersController@restoreUser')->name('restoreUser');
    Route::get('delete-user/{id}', 'UsersController@deleteUser')->name('deleteUser');

    Route::post('/check-user-email', 'UsersController@checkUserEmail')->name('checkUserEmail');


    // Courses Routes
    Route::match(['get', 'post'], '/course/add', 'CoursesController@addCourse')->name('addCourse');
    Route::get('/course/view', 'CoursesController@viewCourses')->name('viewCourses');
    Route::match(['get', 'post'], '/course/edit/{id}', 'CoursesController@editCourse')->name('editCourse');
    Route::get('delete-course/{id}', 'CoursesController@deleteCourse');


    //Sections
    Route::match(['get', 'post'], '/section/add', 'SectionController@addSection')->name('addSection');
    Route::get('/section/view', 'SectionController@viewSections')->name('viewSections');
    Route::match(['get', 'post'], '/section/edit/{id}', 'SectionController@editSection')->name('editSection');
    Route::get('delete-section/{id}', 'SectionController@deleteSection');



    // Batch Routes
    Route::match(['get', 'post'], '/batch/add', 'BatchController@addBatch')->name('addBatch');
    Route::get('/batch/view_b/all', 'BatchController@viewBatches')->name('viewBatches');
    Route::get('delete-batch/{id}', 'BatchController@deleteBatch');
    Route::match(['get', 'post'], '/batch/edit/{id}', 'BatchController@editBatch')->name('editBatch');




    // Site Settings
    Route::get('/site/settings', 'SiteSettingController@siteSettings')->name('siteSettings');
    Route::post('/site/settings/update/{id}', 'SiteSettingController@updateSiteSettings')->name('updateSiteSettings');


});


// Logout

Route::get('/ims/logout', 'AdminLoginController@imsLogout')->name('imsLogout');
