<?php $url = url()->current(); ?>

<div class="sidebar-container">
    <div class="sidemenu-container navbar-collapse collapse fixed-menu">
        <div id="remove-scroll" class="left-sidemenu">
            <ul class="sidemenu  page-header-fixed slimscroll-style" data-keep-expanded="false"
                data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                <li class="sidebar-toggler-wrapper hide">
                    <div class="sidebar-toggler">
                        <span></span>
                    </div>
                </li>
                <?php $current_user = auth()->user(); ?>
                <li class="sidebar-user-panel">
                    <div class="user-panel">
                        <div class="pull-left image">
                            @if($current_user->image == "NULL")
                            <img src="{{ asset('public/uploads/profile/'.$current_user->image) }}" class="img-circle user-img-circle"
                                 alt="{{ $current_user->name }}" />
                                @else
                                <img src="{{ asset('public/uploads/profile/profile.png') }}" class="img-circle user-img-circle"
                                     alt="{{ $current_user->name }}" />
                            @endif
                        </div>
                        <div class="pull-left info">
                            <p> {{ $current_user->name }} </p>
                            <a href="#"><i class="fa fa-circle user-online"></i><span class="txtOnline">
												Online</span></a>
                        </div>
                    </div>
                </li>
                <li class="nav-item start <?php if(preg_match("/dashboard/i", $url)) { echo 'active'; } ?>">
                    <a href="{{ route('admin.dashboard') }}" class="nav-link nav-toggle">
                        <i class="material-icons">dashboard</i>
                        <span class="title">Dashboard</span>
                        <span class="selected"></span>
                    </a>
                </li>

                <li class="nav-item <?php if(preg_match("/user/i", $url)) { echo 'active open'; } ?>">
                    <a href="" class="nav-link nav-toggle"> <i class="material-icons">face</i>
                        <span class="title">User Management</span>
                        <span class="arrow"></span>

                    </a>

                    <ul class="sub-menu">
                        <li class="nav-item <?php if(preg_match("/add/i", $url)) { echo 'active'; } ?>">
                            <a href="{{ route('addUser') }}" class="nav-link ">
                                <span class="title">Add User</span>
                            </a>

                        </li>
                        <li class="nav-item <?php if(preg_match("/users/i", $url)) { echo 'active'; } ?>">
                            <a href="{{ route('viewAllUsers') }}" class="nav-link ">
                                <span class="title">View All Users</span>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="nav-item  <?php if(preg_match("/course/i", $url)) { echo 'active'; } ?>">
                    <a href="{{ route('viewCourses') }}" class="nav-link nav-toggle">
                        <i class="material-icons">school</i>
                        <span class="title">Course</span>
                        <span class="selected"></span>
                    </a>
                </li>

                 <li class="nav-item  <?php if(preg_match("/section/i", $url)) { echo 'active'; } ?>">
                    <a href="{{ route('viewSections') }}" class="nav-link nav-toggle">
                        <i class="material-icons">school</i>
                        <span class="title">Section</span>
                        <span class="selected"></span>
                    </a>
                </li>

                <li class="nav-item  <?php if(preg_match("/batch/i", $url)) { echo 'active'; } ?>">
                    <a href="{{ route('viewBatches') }}" class="nav-link nav-toggle">
                        <i class="material-icons">settings</i>
                        <span class="title">Batch</span>
                        <span class="selected"></span>
                    </a>
                </li>


                <li class="nav-item  <?php if(preg_match("/site/i", $url)) { echo 'active'; } ?>">
                    <a href="{{ route('siteSettings') }}" class="nav-link nav-toggle">
                        <i class="material-icons">settings</i>
                        <span class="title">Site Settings</span>
                        <span class="selected"></span>
                    </a>
                </li>



            </ul>
        </div>
    </div>
</div>
